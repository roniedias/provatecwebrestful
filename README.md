## INSTRUÇÕES ##

Efetue o clone do projeto: *git clone https://roniedias@bitbucket.org/roniedias/provatecwebrestful.git*

Para executá-lo, você precisará previamente possuir o maven (https://maven.apache.org/) instalado em seu ambiente local. Para buildar, execute o comando “mvn clean install” a partir do diretório raiz do projeto. Será gerado o artefato **prova.ear**, em **prova/package/target**. Este é o artefato final a ser deploiado no servidor de aplicação.

Obs: Para execução dos testes, foi utilizado o servidor **Wildfly** (antigo JBoss Community) na versão 8.2.0 (Final), além do SGBD **MySQL**.


Antes de efetuar o deploy, alguns passos se fazem necessários:

1 Configuração do datasource (arquivo arquivo standalone.xml, presente em wildfly-home/standalone/configuration):

Exemplo:

*<datasource jta="true" jndi-name="java:/jdbc/WebrestDS" pool-name="WebrestDS" enabled="true" use-java-context="false">
                    <connection-url>jdbc:mysql://localhost:3306/webrest</connection-url>
                    <driver>mysqlDriver</driver>
                    <transaction-isolation>TRANSACTION_READ_COMMITTED</transaction-isolation>
                    <pool>
                        <min-pool-size>10</min-pool-size>
                        <max-pool-size>20</max-pool-size>
                    </pool>
                    <security>
                        <user-name>root</user-name>
                    </security>
                    <validation>
                        <valid-connection-checker class-name="org.jboss.jca.adapters.jdbc.extensions.mysql.MySQLValidConnectionChecker"/>
                        <validate-on-match>true</validate-on-match>
                        <background-validation>false</background-validation>
                    </validation>
                    <statement>
                        <share-prepared-statements>false</share-prepared-statements>
                    </statement>
                </datasource>*


2 Criação de um banco de dados com o mesmo nome informado na tag “connection-url” do arquivo standalone.xml. Na configuração exemplo acima este nome seria *webrest*.


Foram criados serviços CRUD para as entidades **Cliente** (Cutsomer), **Produto** (Product), **Compra** (Purchase) e **Reserva** (Reservation), além de três serviços com regras específicas. Após o deploy do artefato **prova.ear** no servidor, esses serviços podem ser invocados utilizando-se o verbo http específico para cada um. Segue abaixo url’s exemplo para chamadas a esses serviços:

**Entidade CUSTOMER**

1 - Listagem de todos os clientes (GET): 
*http://localhost/prova/api/rest/customer*

2 - Busca de cliente por id (GET). Neste exemplo, id = 1:
*http://localhost/prova/api/rest/customer/find/1*

3 - Criação de um novo cliente (POST)
*http://localhost/prova/api/rest/customer/create*

*JSON (exemplo) esperado no body*

*{
  "name": "João da Silva Santos",
  "address": "Endereço de Teste",
  "telephone": "(11) 95432 - 3321",
  "status": "ativo"
}
*

4 - Atualização das informações de cliente, a partir de seu código (PUT). Obs.: o número ao final da URL representa o código do cliente
*http://localhost/prova/api/rest/customer/update/1*

*JSON (exemplo) esperado no body*

*{
  "name": “Pedro da Silva Santos",
  "address": "Endereço de Teste alterado”,
  "telephone": "(11) 95432 - 3322”,
  "status": “inativo”
}
*

5 - Exclusão de um cliente, a partir de seu código (DELETE). O número ao final da URL (1) representa o código do cliente
*http://localhost/prova/api/rest/customer/update/1*


6 - Listagem de todos os clientes que possuem reserva
*http://localhost/prova/api/rest/customer/customersWithReservation*


Os exemplos acima elucidam as chamadas aos serviços da entidade Cliente. Para invocar os serviços relacionados com as outras entidades, basta verificar os métodos existentes em seus respectivos controllers. Todos os controllers encontram-se em:  **prova/frontend/web/src/main/java/br/com/prova/web** (vide também comentários logo acima dos métodos presentes nos controllers). 


Obs: Os serviços podem ser acessados fazendo-se uso de ferramentas client para chamadas rest, como o **Postman** ou o **Advanced Rest Client**. Porém, foram criadas páginas para este fim, que se encontram em: **frontend/web/src/main/webapp/pages**. **Nenhuma** validação de input de dados e/ou formatação de layout (design/estética) foi priorizada neste projeto, uma vez que foge a seu propósito. 
Toda a lógica para chamada aos serviços pode ser encontrada no arquivo **script.js** (em **prova/frontend/web/src/main/webapp/js/script.js**).

As implementações dos serviços REST se encontram em **prova/backend/impl/src/main/java/br/com/prova/backend/service**, nas classes **CustomerServiceImpl**, **ProductServiceImpl**, **PurchaseServiceImpl** e **ReservationServiceImpl**