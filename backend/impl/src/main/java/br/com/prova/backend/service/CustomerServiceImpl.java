package br.com.prova.backend.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.prova.backend.entity.Customer;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class CustomerServiceImpl implements CustomerService {

	@PersistenceContext
	private EntityManager em;

	private static final Logger log = LoggerFactory.getLogger(CustomerServiceImpl.class);

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public List<Customer> listCustomers() {
		List<Customer> customers = new ArrayList<Customer>();
		customers = em.createNamedQuery(Customer.QUERY_LIST_ALL_CUSTOMERS, Customer.class).getResultList();
		return customers;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Customer findCustomerById(long customerCode) {
		Customer customer = em.find(Customer.class, customerCode);
		if (customer == null) {
			customer = new Customer();
			customer.setCustomerCode(0);
			customer.setName("Invalid id provided for customer!");
		}
		return customer;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public long createCustomer(Customer customer) {
		long customerCode = 0;
		if (customer != null) {
			em.persist(customer);
			customerCode = customer.getCustomerCode();
		}
		else {
			customerCode = 0;
		}
		return customerCode;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public boolean updateCustomer(long customerCode, Customer customer) {

		boolean updated = false;

		Customer c = em.find(Customer.class, customerCode);
		
		if (c == null) {
			return false; // Invalid data provided in request body
		}

		try {
			c.setAddress(customer.getAddress());
			c.setName(customer.getName());
			c.setStatus(customer.getStatus());
			c.setTelephone(customer.getTelephone());
			em.persist(c);
			updated = true;
		} catch (NullPointerException e) {
			log.info(e.getMessage());
			return false;
		}
		return updated;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public boolean removeCustomer(long customerCode) {

		boolean removed = false;

		Customer c = em.find(Customer.class, customerCode);

		if (c == null) { // Invalid id provided for customer
			return false;
		} else {
			em.remove(c);
			removed = true;
		}

		return removed;
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public List<Customer> customersWithReservation() {
		List<Customer> customersWithReservation = new ArrayList<Customer>();
		List<Customer> allCustomers = new ArrayList<Customer>();
		allCustomers = em.createNamedQuery(Customer.QUERY_LIST_ALL_CUSTOMERS, Customer.class).getResultList();
		for(Customer c : allCustomers) {
			if(c.getReservations().size() > 0) {
				customersWithReservation.add(c);	
			}
		}
		return customersWithReservation;		
	}


}
