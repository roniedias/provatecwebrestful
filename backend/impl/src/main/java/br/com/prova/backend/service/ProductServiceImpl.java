package br.com.prova.backend.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.prova.backend.entity.Product;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class ProductServiceImpl implements ProductService {

	@PersistenceContext
	private EntityManager em;

	private static final Logger log = LoggerFactory.getLogger(CustomerServiceImpl.class);

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public List<Product> listProducts() {
		List<Product> products = new ArrayList<Product>();
		products = em.createNamedQuery(Product.QUERY_LIST_ALL_PRODUCTS, Product.class).getResultList();
		return products;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Product findProductById(long productCode) {
		Product product = em.find(Product.class, productCode);
		if (product == null) {
			product = new Product();
			product.setProductCode(0);
			product.setDescription("Invalid id provided for product!");
		}
		return product;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public long createProduct(Product product) {
		long productCode = 0;
		if (product != null) {
			em.persist(product);
			productCode = product.getProductCode();
		}
		else {
			productCode = 0;
		}
		return productCode;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public boolean updateProduct(long productCode, Product product) {

		boolean updated = false;

		Product p = em.find(Product.class, productCode);
		
		if (p == null) {
			return false; // Invalid data provided in request body
		}

		try {
			p.setDescription(product.getDescription());
			p.setPrice(product.getPrice());
			p.setStock(product.getStock());
			em.persist(p);
			updated = true;
		} catch (NullPointerException e) {
			log.info(e.getMessage());
			return false;
		}
		return updated;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public boolean removeProduct(long productCode) {

		boolean removed = false;

		Product p = em.find(Product.class, productCode);

		if (p == null) { // Invalid id provided for product
			return false;
		} else {
			em.remove(p);
			removed = true;
		}

		return removed;
	}
	
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)	
	public List<Product> listValuesLowerThanOneThousand() {
		List<Product> products = new ArrayList<Product>();
		products = em.createNamedQuery(Product.QUERY_LIST_PRODUCTS_LOWER_THAN_ONE_THOUSAND, Product.class).getResultList();
		return products;		
	}
	


}
