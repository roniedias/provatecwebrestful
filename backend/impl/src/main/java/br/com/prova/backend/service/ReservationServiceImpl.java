package br.com.prova.backend.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.prova.backend.entity.Reservation;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class ReservationServiceImpl implements ReservationService {

	@PersistenceContext
	private EntityManager em;

	private static final Logger log = LoggerFactory.getLogger(CustomerServiceImpl.class);

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public List<Reservation> listReservations() {
		List<Reservation> reservations = new ArrayList<Reservation>();
		reservations = em.createNamedQuery(Reservation.QUERY_LIST_ALL_RESERVATIONS, Reservation.class).getResultList();
		return reservations;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Reservation findReservationById(long reservationCode) {
		Reservation reservation = em.find(Reservation.class, reservationCode);
		if (reservation == null) {
			reservation = new Reservation();
			reservation.setReservationCode(0);
		}
		return reservation;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public long createReservation(Reservation reservation) {
		long reservationCode = 0;
		if (reservation != null) {
			em.persist(reservation);
			reservationCode = reservation.getReservationCode();
		}
		else {
			reservationCode = 0;
		}
		return reservationCode;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public boolean updateReservation(long reservationCode, Reservation reservation) {

		boolean updated = false;

		Reservation r = em.find(Reservation.class, reservationCode);
		
		if (r == null) {
			return false; // Invalid data provided in request body
		}

		try {
			r.setDate(reservation.getDate());
			r.setEmployee(reservation.getEmployee());
			r.setStatus(reservation.getStatus());
			r.setValue(reservation.getValue());
			em.persist(r);
			updated = true;
		} catch (NullPointerException e) {
			log.info(e.getMessage());
			return false;
		}
		return updated;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public boolean removeReservation(long reservationCode) {

		boolean removed = false;

		Reservation r = em.find(Reservation.class, reservationCode);

		if (r == null) { // Invalid id provided for reservation
			return false;
		} else {
			em.remove(r);
			removed = true;
		}

		return removed;
	}


}
