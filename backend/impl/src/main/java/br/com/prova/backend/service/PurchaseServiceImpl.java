package br.com.prova.backend.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.prova.backend.entity.Purchase;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class PurchaseServiceImpl implements PurchaseService {

	@PersistenceContext
	private EntityManager em;

	private static final Logger log = LoggerFactory.getLogger(CustomerServiceImpl.class);

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public List<Purchase> listPurchases() {
		List<Purchase> purchases = new ArrayList<Purchase>();
		purchases = em.createNamedQuery(Purchase.QUERY_LIST_ALL_PURCHASES, Purchase.class).getResultList();
		return purchases;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Purchase findPurchaseById(long purchaseNumber) {
		Purchase purchase = em.find(Purchase.class, purchaseNumber);
		if (purchase == null) {
			purchase = new Purchase();
			purchase.setPurchaseNumber(0);
		}
		return purchase;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public long createPurchase(Purchase purchase) {
		long purchaseNumber = 0;
		if (purchase != null) {
			em.persist(purchase);
			purchaseNumber = purchase.getPurchaseNumber();
		}
		else {
			purchaseNumber = 0;
		}
		return purchaseNumber;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public boolean updatePurchase(long purchaseNumber, Purchase purchase) {

		boolean updated = false;

		Purchase p = em.find(Purchase.class, purchaseNumber);
		
		if (p == null) {
			return false; // Invalid data provided in request body
		}

		try {
			p.setDate(purchase.getDate());
			p.setResponsible(purchase.getResponsible());
			p.setStatus(purchase.getStatus());
			p.setValue(purchase.getValue());
			p.setReservationCode(purchase.getReservationCode());
			em.persist(p);
			updated = true;
		} catch (NullPointerException e) {
			log.info(e.getMessage());
			return false;
		}
		return updated;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public boolean removePurchase(long purchaseNumber) {

		boolean removed = false;

		Purchase p = em.find(Purchase.class, purchaseNumber);

		if (p == null) { // Invalid id provided for purchase
			return false;
		} else {
			em.remove(p);
			removed = true;
		}
		return removed;
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)	
	public List<Purchase> listValuesGreaterThanFiveHundred() {
		List<Purchase> purchases = new ArrayList<Purchase>();
		purchases = em.createNamedQuery(Purchase.QUERY_LIST_PURCHASES_GREATER_THAN_500, Purchase.class).getResultList();
		return purchases;		
	}


}
