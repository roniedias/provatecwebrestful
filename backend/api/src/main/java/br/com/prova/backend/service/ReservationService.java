package br.com.prova.backend.service;

import java.util.List;

import javax.ejb.Local;

import br.com.prova.backend.entity.Reservation;


@Local
public interface ReservationService {
	List<Reservation> listReservations();
	long createReservation(Reservation reservation);
	boolean updateReservation(long reservationCode, Reservation reservation);
	boolean removeReservation(long reservationCode);
	Reservation findReservationById(long reservationCode);
}
