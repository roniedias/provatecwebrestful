package br.com.prova.backend.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@NamedQueries({
	@NamedQuery(name = Purchase.QUERY_LIST_ALL_PURCHASES, query = "SELECT P FROM Purchase p"),
	@NamedQuery(name = Purchase.QUERY_LIST_PURCHASES_GREATER_THAN_500, query = "SELECT P FROM Purchase p WHERE p.value > 500"),
})
public class Purchase {
	
	public static final String QUERY_LIST_ALL_PURCHASES = "QUERY_LIST_ALL_PURCHASES";
	public static final String QUERY_LIST_PURCHASES_GREATER_THAN_500 = "QUERY_LIST_PURCHASES_GREATER_THAN_500";
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long purchaseNumber;
	
	@Column(nullable = false)
	private Date date;
	
	@Column(nullable = false)
	private String responsible;
	
	@Column(nullable = false)
	private String status;
	
	@Column(nullable = false)
	private long value;

	@Column(nullable = false)
    private long reservationCode;

	public long getPurchaseNumber() {
		return purchaseNumber;
	}
	public void setPurchaseNumber(long purchaseNumber) {
		this.purchaseNumber = purchaseNumber;
	}

	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}

	public String getResponsible() {
		return responsible;
	}
	public void setResponsible(String responsible) {
		this.responsible = responsible;
	}

	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	public long getValue() {
		return value;
	}
	public void setValue(long value) {
		this.value = value;
	}

	public long getReservationCode() {
		return reservationCode;
	}
	public void setReservationCode(long reservationCode) {
		this.reservationCode = reservationCode;
	}
	

}
