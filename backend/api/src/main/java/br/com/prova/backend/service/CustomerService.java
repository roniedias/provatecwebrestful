package br.com.prova.backend.service;

import java.util.List;

import javax.ejb.Local;

import br.com.prova.backend.entity.Customer;


@Local
public interface CustomerService {
	List<Customer> listCustomers();
	long createCustomer(Customer customer);
	boolean updateCustomer(long customerCode, Customer customer);
	boolean removeCustomer(long customerCode);
	Customer findCustomerById(long customerCode);
	List<Customer> customersWithReservation();
}
