package br.com.prova.backend.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@NamedQueries({
	@NamedQuery(name = Product.QUERY_LIST_ALL_PRODUCTS, query = "SELECT p FROM Product p"),
	@NamedQuery(name = Product.QUERY_LIST_PRODUCTS_LOWER_THAN_ONE_THOUSAND, query = "SELECT p FROM Product p WHERE p.price < 1000")
})
public class Product {
	
	public static final String QUERY_LIST_ALL_PRODUCTS = "QUERY_LIST_ALL_PRODUCTS";
	public static final String QUERY_LIST_PRODUCTS_LOWER_THAN_ONE_THOUSAND = "QUERY_LIST_PRODUCTS_LOWER_THAN_ONE_THOUSAND";
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long productCode;
	
	@Column(nullable = false)
	private String description;
	
	@Column(nullable = false)
	private double price;
	
	@Column(nullable = false)
	private Integer stock;

	
	public long getProductCode() {
		return productCode;
	}
	public void setProductCode(long productCode) {
		this.productCode = productCode;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}

	public Integer getStock() {
		return stock;
	}
	public void setStock(Integer stock) {
		this.stock = stock;
	}
}