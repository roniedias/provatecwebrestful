package br.com.prova.backend.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

@Entity
@NamedQueries({
	@NamedQuery(name = Customer.QUERY_LIST_ALL_CUSTOMERS, query = "SELECT c FROM Customer c"),
	@NamedQuery(name = Customer.QUERY_LIST_CUSTOMERS_BY_NAME, query = "SELECT c FROM Customer c WHERE c.name = :name")
})
public class Customer {
	
	public static final String QUERY_LIST_ALL_CUSTOMERS = "QUERY_LIST_ALL_CUSTOMERS";
	public static final String QUERY_LIST_CUSTOMERS_BY_NAME = "QUERY_LIST_CUSTOMERS_BY_NAME";
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long customerCode;
	
	@Column(nullable = false)
	private String name;
	
	@Column(nullable = false)
	private String address;
	
	@Column(nullable = false)
	private String telephone;
	
	@Column(nullable = false)
	private String status;
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "customerCode", cascade = CascadeType.ALL)
	private List<Reservation> reservations;
	
	public long getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(long customerCode) {
		this.customerCode = customerCode;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}

	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public List<Reservation> getReservations() {
		return reservations;
	}
	public void setReservations(List<Reservation> reservations) {
		this.reservations = reservations;
	}
	
}