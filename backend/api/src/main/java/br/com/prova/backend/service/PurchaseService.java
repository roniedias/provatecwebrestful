package br.com.prova.backend.service;

import java.util.List;

import javax.ejb.Local;

import br.com.prova.backend.entity.Purchase;

@Local
public interface PurchaseService {
	List<Purchase> listPurchases();
	long createPurchase(Purchase purchase);
	boolean updatePurchase(long purchaseNumber, Purchase purchase);
	boolean removePurchase(long purchaseNumber);
	Purchase findPurchaseById(long purchaseCode);
	List<Purchase> listValuesGreaterThanFiveHundred();
}

