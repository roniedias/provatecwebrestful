package br.com.prova.backend.service;

import java.util.List;

import javax.ejb.Local;

import br.com.prova.backend.entity.Product;


@Local
public interface ProductService {
	List<Product> listProducts();
	long createProduct(Product Product);
	boolean updateProduct(long productCode, Product product);
	boolean removeProduct(long productCode);
	Product findProductById(long productCode);
	List<Product> listValuesLowerThanOneThousand();
}
