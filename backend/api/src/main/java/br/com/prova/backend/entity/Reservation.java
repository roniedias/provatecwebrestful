package br.com.prova.backend.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

@Entity
@NamedQueries({
	@NamedQuery(name = Reservation.QUERY_LIST_ALL_RESERVATIONS, query = "SELECT r FROM Reservation r")
})
public class Reservation {
	
	public static final String QUERY_LIST_ALL_RESERVATIONS = "QUERY_LIST_ALL_RESERVATIONS";
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long reservationCode;
	
	@Column(nullable = false)
	private Date date;
	
	@Column(nullable = false)
	private String employee;
	
	@Column(nullable = false)
	private String status;
	
	@Column(nullable = false)
	private long value;
	
	@Column(nullable = false)
	private long customerCode;
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "reservationCode", cascade = CascadeType.ALL)
	private List<Purchase> purchases;


	public long getReservationCode() {
		return reservationCode;
	}
	public void setReservationCode(long reservationCode) {
		this.reservationCode = reservationCode;
	}

	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}

	public String getEmployee() {
		return employee;
	}
	public void setEmployee(String employee) {
		this.employee = employee;
	}

	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	public long getValue() {
		return value;
	}
	public void setValue(long value) {
		this.value = value;
	}
	
	public long getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(long customerCode) {
		this.customerCode = customerCode;
	}
	
	public List<Purchase> getPurchases() {
		return purchases;
	}
	public void setPurchases(List<Purchase> purchases) {
		this.purchases = purchases;
	}
	

}