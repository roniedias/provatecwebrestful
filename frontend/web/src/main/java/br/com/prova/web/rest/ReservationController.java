package br.com.prova.web.rest;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.prova.backend.entity.Reservation;
import br.com.prova.backend.service.ReservationService;


@Path("reservation")
@Produces(MediaType.APPLICATION_JSON)
public class ReservationController {
	
	@EJB
	private ReservationService reservationService;
	
	// http://localhost/prova/api/rest/reservation
	@GET
	@Path("")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response listReservations() throws Exception {
		return Response.status(Status.OK).entity(reservationService.listReservations()).build();
	}	
	
	// http://localhost/prova/api/rest/reservation/find/1
	@GET 
	@Path("/find/{reservationCode}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response findReserationById(@PathParam("reservationCode") long reservationCode) throws Exception {
		Reservation reservation = reservationService.findReservationById(reservationCode);
		long r = reservation.getReservationCode();
		if(r == 0) {
			return Response.status(Status.BAD_REQUEST).entity(reservationService.findReservationById(reservationCode)).build();
		}
		return Response.status(Status.OK).entity(reservationService.findReservationById(reservationCode)).build();
	}

	
	
	//http://localhost/prova/api/rest/reservation/create
	/* In body:
	{
	"date": 1457738317197,
	"employee": "Teodoro de Souza",
	"status": "Ativo",
	"value": 30.00,
 	"customerCode": 1
	}	 
	*/
	@POST
	@Path("/create")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createReservation(Reservation reservation) throws Exception {
		long reservationCode = reservationService.createReservation(reservation);
		if(reservationCode == 0) {
			return Response.status(Status.BAD_REQUEST).entity("{\"id\": " + reservationCode + "}").build();
		}
		return Response.status(Status.OK).entity("{\"id\": " + reservationCode + "}").build();
	}
	
	
	//http://localhost/prova/api/rest/reservation/update/1
	/* In body:
	 * {
		"date": 1457738317197,
		"employee": "Teodoro de Souza Santos",
		"status": "Inativo",
		"value": 50.00
	   }
	 */
	@PUT
	@Path("/update/{reservationCode}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateReservation(@PathParam("reservationCode") long reservationCode, Reservation reservation) throws Exception {
		boolean updated = reservationService.updateReservation(reservationCode, reservation);
		if(!updated) {
			return Response.status(Status.BAD_REQUEST).entity("{\"update\": " +  updated + "}").build();
		}
		return Response.status(Status.OK).entity("{\"update\": " +  updated + "}").build();
	}
	
	
	//http://localhost/prova/api/rest/reservation/remove/3
	@DELETE
	@Path("/remove/{reservationCode}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response removeReservation(@PathParam("reservationCode") long reservationCode) throws Exception {
		boolean deleted = reservationService.removeReservation(reservationCode);
		if(!deleted) {
			return Response.status(Status.BAD_REQUEST).entity("{\"delete\": " + deleted + "}").build();
		}
		return Response.status(Status.OK).entity("{\"delete\": " + deleted + "}").build();
	}

	
}
