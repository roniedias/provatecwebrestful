package br.com.prova.web.rest;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.prova.backend.entity.Product;
import br.com.prova.backend.service.ProductService;


@Path("product")
@Produces(MediaType.APPLICATION_JSON)
public class ProductController {
	
	@EJB
	private ProductService productService;
	
	// http://localhost/prova/api/rest/product
	@GET
	@Path("")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response listProducts() throws Exception {
		return Response.status(Status.OK).entity(productService.listProducts()).build();
	}	
	
	// http://localhost/prova/api/rest/product/find/1
	@GET 
	@Path("/find/{productCode}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response findProductById(@PathParam("productCode") long productCode) throws Exception {
		Product product = productService.findProductById(productCode);
		long p = product.getProductCode();
		if(p == 0) {
			return Response.status(Status.BAD_REQUEST).entity(productService.findProductById(productCode)).build();
		}
		return Response.status(Status.OK).entity(productService.findProductById(productCode)).build();
	}

	
	
	//http://localhost/prova/api/rest/product/create
	/* In body:
	 * {
		"description": "Produto 1",
		"price": 60.00,
		"stock": 15
	   }
	 */
	@POST
	@Path("/create")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createProduct(Product product) throws Exception {
		long productCode = productService.createProduct(product);
		if(productCode == 0) {
			return Response.status(Status.BAD_REQUEST).entity("{\"id\": " + productCode + "}").build();
		}
		return Response.status(Status.OK).entity("{\"id\": " + productCode + "}").build();
	}
	
	
	//http://localhost/prova/api/rest/product/update/1
	/* In body:
	 * {
		"description": "Produto 1 - Atualizado",
		"price": 65.00,
		"stock": 13
	   }
	 */
	@PUT
	@Path("/update/{productCode}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateProduct(@PathParam("productCode") long productCode, Product product) throws Exception {
		boolean updated = productService.updateProduct(productCode, product);
		if(!updated) {
			return Response.status(Status.BAD_REQUEST).entity("{\"update\": " +  updated + "}").build();
		}
		return Response.status(Status.OK).entity("{\"update\": " +  updated + "}").build();
	}
	
	
	//http://localhost/prova/api/rest/product/remove/6
	@DELETE
	@Path("/remove/{productCode}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response removeProduct(@PathParam("productCode") long productCode) throws Exception {
		boolean deleted = productService.removeProduct(productCode);
		if(!deleted) {
			return Response.status(Status.BAD_REQUEST).entity("{\"delete\": " + deleted + "}").build();
		}
		return Response.status(Status.OK).entity("{\"delete\": " + deleted + "}").build();
	}
	
	
	
	// http://localhost/prova/api/rest/product/lowerthanonethousand
	@GET
	@Path("lowerthanonethousand")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response lowerThanOneThousand() throws Exception {
		return Response.status(Status.OK).entity(productService.listValuesLowerThanOneThousand()).build();
	}	


	
}

