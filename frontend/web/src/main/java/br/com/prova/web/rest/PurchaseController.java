package br.com.prova.web.rest;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.prova.backend.entity.Purchase;
import br.com.prova.backend.service.PurchaseService;


@Path("purchase")
@Produces(MediaType.APPLICATION_JSON)
public class PurchaseController {
	
	@EJB
	private PurchaseService purchaseService;
	
	// http://localhost/prova/api/rest/purchase
	@GET
	@Path("")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response listPurchases() throws Exception {
		return Response.status(Status.OK).entity(purchaseService.listPurchases()).build();
	}	
	
	// http://localhost/prova/api/rest/purchase/find/1
	@GET 
	@Path("/find/{purchaseNumber}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response findReserationById(@PathParam("purchaseNumber") long purchaseNumber) throws Exception {
		Purchase purchase = purchaseService.findPurchaseById(purchaseNumber);
		long p = purchase.getPurchaseNumber();
		if(p == 0) {
			return Response.status(Status.BAD_REQUEST).entity(purchaseService.findPurchaseById(purchaseNumber)).build();
		}
		return Response.status(Status.OK).entity(purchaseService.findPurchaseById(purchaseNumber)).build();
	}

	
	
	//http://localhost/prova/api/rest/purchase/create
	/* In body:
	{
	"date": 1457738317197,
	"responsible": "Teofronio Assis",
	"status": "Ativo",
	"value": 30.00,
 	"reservationCode": 1
	}	*/
	@POST
	@Path("/create")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createPurchase(Purchase purchase) throws Exception {
		long purchaseNumber = purchaseService.createPurchase(purchase);
		if(purchaseNumber == 0) {
			return Response.status(Status.BAD_REQUEST).entity("{\"id\": " + purchaseNumber + "}").build();
		}
		return Response.status(Status.OK).entity("{\"id\": " + purchaseNumber + "}").build();
	}
	
	
	//http://localhost/prova/api/rest/purchase/update/1
	/* In body:
	 * {
		"date": 1457738317197,
		"responsible": "Teofronio Assis de Souza",
		"status": "Inativo por inadimplência",
		"value": 35.50,
 		"reservationCode": 9
		}
	 */
	@PUT
	@Path("/update/{purchaseNumber}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updatePurchase(@PathParam("purchaseNumber") long purchaseNumber, Purchase purchase) throws Exception {
		boolean updated = purchaseService.updatePurchase(purchaseNumber, purchase);
		if(!updated) {
			return Response.status(Status.BAD_REQUEST).entity("{\"update\": " +  updated + "}").build();
		}
		return Response.status(Status.OK).entity("{\"update\": " +  updated + "}").build();
	}
	
	
	//http://localhost/prova/api/rest/purchase/remove/3
	@DELETE
	@Path("/remove/{purchaseNumber}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response removePurchase(@PathParam("purchaseNumber") long purchaseNumber) throws Exception {
		boolean deleted = purchaseService.removePurchase(purchaseNumber);
		if(!deleted) {
			return Response.status(Status.BAD_REQUEST).entity("{\"delete\": " + deleted + "}").build();
		}
		return Response.status(Status.OK).entity("{\"delete\": " + deleted + "}").build();
	}
	
	// http://localhost/prova/api/rest/purchase/greaterthanfivehundred
	@GET
	@Path("greaterthanfivehundred")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response greaterThanFiveHundred() throws Exception {
		return Response.status(Status.OK).entity(purchaseService.listValuesGreaterThanFiveHundred()).build();
	}	
	

	
}
