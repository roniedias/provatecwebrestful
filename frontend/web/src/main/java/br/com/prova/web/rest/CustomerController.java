package br.com.prova.web.rest;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.prova.backend.entity.Customer;
import br.com.prova.backend.service.CustomerService;


@Path("customer")
@Produces(MediaType.APPLICATION_JSON)
public class CustomerController {
	
	@EJB
	private CustomerService customerService;
	
	// http://localhost/prova/api/rest/customer
	@GET
	@Path("")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response listCustomers() throws Exception {
		return Response.status(Status.OK).entity(customerService.listCustomers()).build();
	}	
	
	// http://localhost/prova/api/rest/customer/find/1
	@GET 
	@Path("/find/{customerCode}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response findCustomerById(@PathParam("customerCode") long customerCode) throws Exception {
		Customer customer = customerService.findCustomerById(customerCode);
		long c = customer.getCustomerCode();
		if(c == 0) {
			return Response.status(Status.BAD_REQUEST).entity(customerService.findCustomerById(customerCode)).build();
		}
		return Response.status(Status.OK).entity(customerService.findCustomerById(customerCode)).build();
	}

	
	
	//http://localhost/prova/api/rest/customer/create
	/* In body:
	 * {
		"name": "João da Silva Santos",
		"address": "Endereço de Teste",
		"telephone": "(11) 95432 - 3321",
		"status": "ativo"
	   }
	 */
	@POST
	@Path("/create")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createCustomer(Customer customer) throws Exception {
		long customerCode = customerService.createCustomer(customer);
		if(customerCode == 0) {
			return Response.status(Status.BAD_REQUEST).entity("{\"id\": " + customerCode + "}").build();
		}
		return Response.status(Status.OK).entity("{\"id\": " + customerCode + "}").build();
	}
	
	
	//http://localhost/prova/api/rest/customer/update/1
	/* In body:
	 * {
		"name": "João da Silva Santos alerado",
		"address": "Endereço de Teste alterado",
		"telephone": "(11) 93321-5432",
		"status": "inativo"
	   }
	 */
	@PUT
	@Path("/update/{customerCode}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateCustomer(@PathParam("customerCode") long customerCode, Customer customer) throws Exception {
		boolean updated = customerService.updateCustomer(customerCode, customer);
		if(!updated) {
			return Response.status(Status.BAD_REQUEST).entity("{\"update\": " +  updated + "}").build();
		}
		return Response.status(Status.OK).entity("{\"update\": " +  updated + "}").build();
	}
	
	
	//http://localhost/prova/api/rest/customer/remove/3
	@DELETE
	@Path("/remove/{customerCode}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response removeCustomer(@PathParam("customerCode") long customerCode) throws Exception {
		boolean deleted = customerService.removeCustomer(customerCode);
		if(!deleted) {
			return Response.status(Status.BAD_REQUEST).entity("{\"delete\": " + deleted + "}").build();
		}
		return Response.status(Status.OK).entity("{\"delete\": " + deleted + "}").build();
	}
	
	
	// http://localhost/prova/api/rest/customer/customersWithReservation
	@GET
	@Path("customersWithReservation")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response listCustomersWithReservation() throws Exception {
		return Response.status(Status.OK).entity(customerService.customersWithReservation()).build();
	}	


	
}
