package br.com.prova.web.config;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("api/rest")
public class RestConfig extends Application {
	
}

