$( document ).ready(function() {


/* =============  CUSTOMER'S METHODS  ================================================== */


	// Customer creation
	$( "#create-customer-submit-button" ).click(function() {
  		var url = "http://localhost/prova/api/rest/customer/create";
  		var data = {name: String($("#create-customer-input-name").val()), address: String($("#create-customer-input-address").val()), telephone: String($("#create-customer-telephone").val()), status: String($("#create-customer-status").val())};
  		$.ajax({
  			type: "POST",
  			url: url,
  			contentType:"application/json",
  			crossDomain: true,
  			success: function(result){
  				alert("Inclusão efetuada com sucesso!");
  				window.location = "http://localhost/prova";
 			 },
 			 error:function(xhr,status,error){
 			 	alert(xhr.responseText);
        	},
        	data: JSON.stringify(data),
        	dataType: 'json'
		});
  	});


	// List all customers
	$( "#list-all-customers-button" ).click(function() {
		$.getJSON("http://localhost/prova/api/rest/customer",
    		function (jsonResult) {
	        	var tr;
	        	for (var i = 0; i < jsonResult.length; i++) {
	            	tr = $('<tr/>');
	            	tr.append("<td>" + jsonResult[i].name + "</td>");
	            	tr.append("<td>" + jsonResult[i].address + "</td>");
	            	tr.append("<td>" + jsonResult[i].telephone + "</td>");
	            	tr.append("<td>" + jsonResult[i].status + "</td>");
	            	$('#list-all-customers-table').append(tr);
	        	}
	    	});
	});
	$( "#list-customers-back-button" ).click(function() {
			window.location = "http://localhost/prova";
	});


		// List customers with reservation
	$( "#list-customers-with-reservation-button" ).click(function() {
		$.getJSON("http://localhost/prova/api/rest/customer/customersWithReservation",
    		function (jsonResult) {
	        	var tr;
	        	for (var i = 0; i < jsonResult.length; i++) {
	            	tr = $('<tr/>');
	            	tr.append("<td>" + jsonResult[i].name + "</td>");
	            	tr.append("<td>" + jsonResult[i].address + "</td>");
	            	tr.append("<td>" + jsonResult[i].telephone + "</td>");
	            	tr.append("<td>" + jsonResult[i].status + "</td>");
	            	$('#list-customers-with-reservation-table').append(tr);
	        	}
	    	});
	});
	$( "#list-customers-with-reservation-back-button" ).click(function() {
			window.location = "http://localhost/prova";
	});


	// Remove customer
	$( "#remove-customer-submit-button" ).click(function() {
  		var url = "http://localhost/prova/api/rest/customer/remove/";		
  		$.ajax({
  			type: "DELETE",
  			url: url + String($("#remove-customer-input").val()),
  			success: function(result){
  				alert("Remoção efetuada com sucesso!");
  				window.location = "http://localhost/prova";
 			 },
 			 error:function(xhr,status,error){
 			 	alert(xhr.responseText);
        	},
		});
  	});


	// Update customer
	$( "#update-customer-submit-button" ).click(function() {
  		var url = "http://localhost/prova/api/rest/customer/update/";
  		var data = {name: String($("#update-customer-input-name").val()), address: String($("#update-customer-input-address").val()), telephone: String($("#update-customer-telephone").val()), status: String($("#update-customer-status").val())};
  		$.ajax({
  			type: "PUT",
  			url: url + String($("#update-customer-input-id").val()),
  			contentType:"application/json",
  			crossDomain: true,
  			success: function(result){
  				alert("Atualização efetuada com sucesso!");
  				window.location = "http://localhost/prova";
 			 },
 			 error:function(xhr,status,error){
 			 	alert(xhr.responseText);
        	},
        	data: JSON.stringify(data),
        	dataType: 'json'
		});
  	});



/* ===================================================================================== */	



/* =============  RESERVATION'S METHODS  ================================================== */


	// Reservation creation
	$( "#create-reservation-submit-button" ).click(function() {
  		var url = "http://localhost/prova/api/rest/reservation/create";
  		var data = {date: String($("#create-reservation-input-date").val()), employee: String($("#create-reservation-input-employee").val()), value: String($("#create-reservation-input-value").val()), status: String($("#create-reservation-input-status").val()), customerCode: String($("#create-reservation-input-customer-code").val())};
  		$.ajax({
  			type: "POST",
  			url: url,
  			contentType:"application/json",
  			crossDomain: true,
  			success: function(result){
  				alert("Inclusão da reserva efetuada com sucesso!");
  				window.location = "http://localhost/prova";
 			 },
 			 error:function(xhr,status,error){
 			 	alert(xhr.responseText);
        	},
        	data: JSON.stringify(data),
        	dataType: 'json'
		});
  	});


	// List all reservations
	$( "#list-all-reservations-button" ).click(function() {
		$.getJSON("http://localhost/prova/api/rest/reservation",
    		function (jsonResult) {
	        	var tr;
	        	for (var i = 0; i < jsonResult.length; i++) {
	            	tr = $('<tr/>');
	            	tr.append("<td>" + jsonResult[i].date + "</td>");
	            	tr.append("<td>" + jsonResult[i].employee + "</td>");
	            	tr.append("<td>" + jsonResult[i].value + "</td>");
	            	tr.append("<td>" + jsonResult[i].status + "</td>");
	            	$('#list-all-reservations-table').append(tr);
	        	}
	    	});
	});
	$( "#list-reservations-back-button" ).click(function() {
			window.location = "http://localhost/prova";
	});


	// Remove reservation
	$( "#remove-reservation-submit-button" ).click(function() {
  		var url = "http://localhost/prova/api/rest/reservation/remove/";		
  		$.ajax({
  			type: "DELETE",
  			url: url + String($("#remove-reservation-input").val()),
  			success: function(result){
  				alert("Remoção da reserva efetuada com sucesso!");
  				window.location = "http://localhost/prova";
 			 },
 			 error:function(xhr,status,error){
 			 	alert(xhr.responseText);
        	},
		});
  	});


	// Update reservation
	$( "#update-reservation-submit-button" ).click(function() {
  		var url = "http://localhost/prova/api/rest/reservation/update/";
  		var data = {date: String($("#update-purchase-input-date").val()), employee: String($("#update-purchase-input-employee").val()), value: String($("#update-purchase-input-value").val()), status: String($("#update-purchase-input-status").val()), customerCode: String($("#update-purchase-input-customer-code").val())};
  		$.ajax({
  			type: "PUT",
  			url: url + String($("#update-reservation-input-id").val()),
  			contentType:"application/json",
  			crossDomain: true,
  			success: function(result){
  				alert("Atualização da reserva efetuada com sucesso!");
  				window.location = "http://localhost/prova";
 			 },
 			 error:function(xhr,status,error){
 			 	alert(xhr.responseText);
        	},
        	data: JSON.stringify(data),
        	dataType: 'json'
		});
  	});



/* ===================================================================================== */	



/* =============  PRODUCT'S METHODS  ================================================== */


	// Product creation
	$( "#create-product-submit-button" ).click(function() {
  		var url = "http://localhost/prova/api/rest/product/create";
  		var data = {description: String($("#create-product-input-description").val()), price: parseFloat($("#create-product-input-price").val()), stock: parseInt($("#create-product-input-stock").val())};
  		$.ajax({
  			type: "POST",
  			url: url,
  			contentType:"application/json",
  			crossDomain: true,
  			success: function(result){
  				alert("Inclusão do produto efetuada com sucesso!");
  				window.location = "http://localhost/prova";
 			 },
 			 error:function(xhr,status,error){
 			 	alert(xhr.responseText);
        	},
        	data: JSON.stringify(data),
        	dataType: 'json'
		});
  	});


	// List all products
	$( "#list-all-products-button" ).click(function() {
		$.getJSON("http://localhost/prova/api/rest/product",
    		function (jsonResult) {
	        	var tr;
	        	for (var i = 0; i < jsonResult.length; i++) {
	            	tr = $('<tr/>');
	            	tr.append("<td>" + jsonResult[i].description + "</td>");
	            	tr.append("<td>" + jsonResult[i].price + "</td>");
	            	tr.append("<td>" + jsonResult[i].stock + "</td>");
	            	$('#list-all-products-table').append(tr);
	        	}
	    	});
	});
	$( "#list-products-back-button" ).click(function() {
			window.location = "http://localhost/prova";
	});


	// Remove product
	$( "#remove-product-submit-button" ).click(function() {
  		var url = "http://localhost/prova/api/rest/product/remove/";		
  		$.ajax({
  			type: "DELETE",
  			url: url + String($("#remove-product-input").val()),
  			success: function(result){
  				alert("Remoção do produto efetuada com sucesso!");
  				window.location = "http://localhost/prova";
 			 },
 			 error:function(xhr,status,error){
 			 	alert(xhr.responseText);
        	},
		});
  	});


	// Update product
	$( "#update-product-submit-button" ).click(function() {
  		var url = "http://localhost/prova/api/rest/product/update/";
  		var data = {description: String($("#update-product-input-description").val()), price: parseFloat($("#update-product-input-price").val()), stock: parseInt($("#update-product-input-stock").val())};
  		$.ajax({
  			type: "PUT",
  			url: url + String($("#update-product-input-id").val()),
  			contentType:"application/json",
  			crossDomain: true,
  			success: function(result){
  				alert("Atualização da reserva efetuada com sucesso!");
  				window.location = "http://localhost/prova";
 			 },
 			 error:function(xhr,status,error){
 			 	alert(xhr.responseText);
        	},
        	data: JSON.stringify(data),
        	dataType: 'json'
		});
  	});


	// List all products
	$( "#list-products-lowerthanonethousand-button" ).click(function() {
		$.getJSON("http://localhost/prova/api/rest/product/lowerthanonethousand",
    		function (jsonResult) {
	        	var tr;
	        	for (var i = 0; i < jsonResult.length; i++) {
	            	tr = $('<tr/>');
	            	tr.append("<td>" + jsonResult[i].description + "</td>");
	            	tr.append("<td>" + jsonResult[i].price + "</td>");
	            	tr.append("<td>" + jsonResult[i].stock + "</td>");
	            	$('#list-products-lowerthanonethousand-table').append(tr);
	        	}
	    	});
	});
	$( "#list-products-lowerthanonethousand-back-button" ).click(function() {
			window.location = "http://localhost/prova";
	});




/* ===================================================================================== */	



/* =============  PURCHASE'S METHODS  ================================================== */


	// Purchase creation
	$( "#create-purchase-submit-button" ).click(function() {
  		var url = "http://localhost/prova/api/rest/purchase/create";
  		var data = {date: String($("#create-purchase-input-date").val()), responsible: String($("#create-purchase-input-responsible").val()), value: String($("#create-purchase-input-value").val()), status: String($("#create-purchase-input-status").val()), reservationCode: String($("#create-purchase-input-reservation-code").val())};
  		$.ajax({
  			type: "POST",
  			url: url,
  			contentType:"application/json",
  			crossDomain: true,
  			success: function(result){
  				alert("Compra efetuada com sucesso!");
  				window.location = "http://localhost/prova";
 			 },
 			 error:function(xhr,status,error){
 			 	alert(xhr.responseText);
        	},
        	data: JSON.stringify(data),
        	dataType: 'json'

		});
  });


	// List all purchases
	$( "#list-all-purchases-button" ).click(function() {
		$.getJSON("http://localhost/prova/api/rest/purchase",
    		function (jsonResult) {
	        	var tr;
	        	for (var i = 0; i < jsonResult.length; i++) {
	            	tr = $('<tr/>');
	            	tr.append("<td>" + jsonResult[i].date + "</td>");
	            	tr.append("<td>" + jsonResult[i].responsible + "</td>");
	            	tr.append("<td>" + jsonResult[i].value + "</td>");
	            	tr.append("<td>" + jsonResult[i].status + "</td>");
	            	tr.append("<td>" + jsonResult[i].reservationCode + "</td>");
	            	$('#list-all-purchases-table').append(tr);
	        	}
	    	});
	});
	$( "#list-purchases-back-button" ).click(function() {
			window.location = "http://localhost/prova";
	});


	// Remove purchase
	$( "#remove-purchase-button" ).click(function() {
  		var url = "http://localhost/prova/api/rest/purchase/remove/";		
  		$.ajax({
  			type: "DELETE",
  			url: url + String($("#remove-purchase-input").val()),
  			success: function(result){
  				alert("Remoção da compra efetuada com sucesso!");
  				window.location = "http://localhost/prova";
 			 },
 			 error:function(xhr,status,error){
 			 	alert(xhr.responseText);
        	},
		});
  });


	// Update purchase
	$( "#update-purchase-submit-button" ).click(function() {
  		var url = "http://localhost/prova/api/rest/purchase/update/";
  		var data = {date: String($("#update-purchase-input-date").val()), responsible: String($("#update-purchase-input-responsible").val()), value: String($("#update-purchase-input-value").val()), status: String($("#update-purchase-input-status").val()), reservationCode: String($("#update-purchase-input-reservation-code").val())};
  		$.ajax({
  			type: "PUT",
  			url: url + String($("#update-purchase-input-id").val()),
  			contentType:"application/json",
  			crossDomain: true,
  			success: function(result){
  				alert("Atualização da compra efetuada com sucesso!");
  				window.location = "http://localhost/prova";
 			 },
 			 error:function(xhr,status,error){
 			 	alert(xhr.responseText);
        	},
        	data: JSON.stringify(data),
        	dataType: 'json'
		});
  });



  // List purchases greater than five hundred
  $( "#list-purchases-greater-than-five-hundred-submit-button" ).click(function() {
    $.getJSON("http://localhost/prova/api/rest/purchase/greaterthanfivehundred",
        function (jsonResult) {
            var tr;
            for (var i = 0; i < jsonResult.length; i++) {
                tr = $('<tr/>');
                tr.append("<td>" + jsonResult[i].date + "</td>");
                tr.append("<td>" + jsonResult[i].responsible + "</td>");
                tr.append("<td>" + jsonResult[i].value + "</td>");
                tr.append("<td>" + jsonResult[i].status + "</td>");
                tr.append("<td>" + jsonResult[i].reservationCode + "</td>");
                $('#list-purchases-greater-than-five-hundred-table').append(tr);
            }
        });
        
  });
  $( "#list-purchases-greater-than-five-hundred-back-button" ).click(function() {
      window.location = "http://localhost/prova";
  });


/* ===================================================================================== */	
	


});



